const WIDTH = 800;
const HEIGHT = 500;
const canvas = document.getElementById('canvas');
canvas.width = WIDTH;
canvas.height = HEIGHT;

const array = [];
const N = 100;

class Ctx {
    constructor(ctx) {
        this.ctx = ctx;
    }

    drawRect(rectangle, color) {
        if (!(arguments.length >= 1 && arguments.length <= 2)) {
            throw new DataError('Wrong amount of arguments');
        } else if (!(rectangle instanceof Rectangle)) {
            throw new DataError('Argument is not an instance of Rectangle');
        }

        this.ctx.strokeStyle = color;
        this.ctx.strokeRect(rectangle.x1, rectangle.y1, rectangle.width, rectangle.height);
    }

    drawEllipse(ellipse, color) {
        if (!(arguments.length >= 1 && arguments.length <= 2)) {
            throw new DataError('Wrong amount of arguments');
        } else if (!(ellipse instanceof Ellipse)) {
            throw new DataError('Argument is not an instance of Ellipse');
        }

        this.ctx.beginPath();
        this.ctx.strokeStyle = color;
        this.ctx.ellipse(ellipse.cx, ellipse.cy, ellipse.rx, ellipse.ry, Math.PI, 0, 2 * Math.PI);
        this.ctx.stroke();
    }

    drawPoint(point, color) {
        if (!(arguments.length >= 1 && arguments.length <= 2)) {
            throw new DataError('Wrong amount of arguments');
        } else if (!(point instanceof Point)) {
            throw new DataError('Argument is not an instance of Point');
        }

        this.ctx.beginPath();
        this.ctx.strokeStyle = color;
        this.ctx.arc(point.x, point.y, 1, 0, 2 * Math.PI);
        this.ctx.stroke();
    }
}

class DataError extends Error {
    constructor(message) {
        super(message);

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, DataError);
        }
        this.name = 'DataError';
    }
}

class Point {
    constructor(x, y) {
        if (arguments.length !== 2) {
            throw new DataError('Wrong amount of arguments');
        } else if (typeof x !== 'number' || typeof y !== 'number') {
            throw new DataError('Argument is not a number');
        }
        this.x = x;
        this.y = y;
    }

    static randomInteger(min, max) {
        if (arguments.length !== 2) {
            throw new DataError('Wrong amount of arguments');
        } else if (typeof min !== 'number' || typeof max !== 'number') {
            throw new DataError('Argument is not a number');
        }
        return Math.floor(min + Math.random() * (max + 1 - min));
    }

    static random(xFrom, xTo, yFrom, yTo) {
        if (arguments.length !== 4) {
            throw new DataError('Wrong amount of arguments');
        } else if (typeof xFrom !== 'number' || typeof xTo !== 'number' || typeof yFrom !== 'number' || typeof yTo !== 'number') {
            throw new DataError('Argument is not a number');
        }
        return new Point(Point.randomInteger(xFrom, xTo), Point.randomInteger(yFrom, yTo));
    }
}

class Line {
    constructor(point1, point2) {
        if (arguments.length !== 2) {
            throw new DataError('Wrong amount of arguments');
        } else if (!(point1 instanceof Point) || !(point2 instanceof Point)) {
            throw new DataError('Argument is not an instance of Point');
        }

        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = point2.x;
        this.y2 = point2.y;
    }

    get length() {
        return Math.sqrt((this.x2 - this.x1) ** 2 + (this.y2 - this.y1) ** 2);
    }
}

class Rectangle {
    constructor(point1, point2) {
        if (arguments.length !== 2) {
            throw new DataError('Wrong amount of arguments');
        } else if (!(point1 instanceof Point) || !(point2 instanceof Point)) {
            throw new DataError('Argument is not an instance of Point');
        }

        this.x1 = Math.min(point1.x, point2.x);
        this.y1 = Math.min(point1.y, point2.y);
        this.x2 = Math.max(point1.x, point2.x);
        this.y2 = Math.max(point1.y, point2.y);
        this.width = this.x2 - this.x1;
        this.height = this.y2 - this.y1;
    }

    isSquare() {
        return this.width === this.height;
    }

    get area() {
        return this.width * this.height;
    }

    contains(point) {
        if (arguments.length !== 1) {
            throw new DataError('Wrong amount of arguments');
        } else if (!(point instanceof Point)) {
            throw new DataError('Argument is not an instance of Point');
        }

        return point.x > this.x1 && point.x < this.x2 && point.y > this.y1 && point.y < this.y2;
    }

    intersect(rect) {
        if (arguments.length !== 1) {
            throw new DataError('Wrong amount of arguments');
        } else if (!(rect instanceof Rectangle)) {
            throw new DataError('Argument is not an instance of Rectangle');
        }

        const x1 = Math.max(this.x1, rect.x1);
        const y1 = Math.max(this.y1, rect.y1);
        const x2 = Math.min(this.x2, rect.x2);
        const y2 = Math.min(this.y2, rect.y2);
        const width = x2 - x1;
        const height = y2 - y1;
        if (width >= 0 && height >= 0) return new Rectangle(new Point(x1, y1), new Point(x2, y2));
    }
}


class Ellipse {
    constructor(cx, cy, rx, ry) {
        if (arguments.length !== 4) {
            throw new DataError('Wrong amount of arguments');
        } else if (typeof cx !== 'number' || typeof cy !== 'number' || typeof rx !== 'number' || typeof ry !== 'number') {
            throw new DataError('Argument is not a number');
        }

        this.cx = cx;
        this.cy = cy;
        this.rx = rx;
        this.ry = ry;
    }

    generatePoints(arr, n) {
        if (arguments.length !== 2) {
            throw new DataError('Wrong amount of arguments');
        } else if (!Array.isArray(arr)) {
            throw new DataError('Argument "arr" is not an array');
        } else if (typeof n !== 'number') {
            throw new DataError('Argument "n" is not a number');
        }

        for (let i = 0; i < n; i++) {
            const t = 2 * Math.PI * Math.random();
            const d = Math.sqrt(Math.random());
            const x = this.cx + this.rx * d * Math.cos(t);
            const y = this.cy + this.ry * d * Math.sin(t);
            arr.push(new Point(x, y));
        }
    }

    isCircle() {
        return this.rx === this.ry;
    }

    get area() {
        return this.rx * this.ry * Math.PI;
    }

    contains(point) {
        if (arguments.length !== 1) {
            throw new DataError('Wrong amount of arguments');
        } else if (!(point instanceof Point)) {
            throw new DataError('Argument is not an instance of Point');
        }

        return (this.cx - point.x) ** 2 / this.rx ** 2 + (this.cy - point.y) ** 2 / this.ry ** 2 < 1;
    }
}

try {
    const rectangle1 = new Rectangle(Point.random(0, WIDTH, 0, HEIGHT), Point.random(0, WIDTH, 0, HEIGHT));
    const rectangle2 = new Rectangle(Point.random(0, WIDTH, 0, HEIGHT), Point.random(0, WIDTH, 0, HEIGHT));

    const ctx = new Ctx(canvas.getContext('2d'));
    ctx.drawRect(rectangle1);
    ctx.drawRect(rectangle2);

    const rectangle3 = rectangle1.intersect(rectangle2);

    if (rectangle3 !== undefined) {

        ctx.drawRect(rectangle3, 'red');

        const ellipse1 = new Ellipse(((rectangle3.x2 - rectangle3.x1) / 2) + rectangle3.x1,
            ((rectangle3.y2 - rectangle3.y1) / 2) + rectangle3.y1,
            (rectangle3.x2 - rectangle3.x1) / 2,
            (rectangle3.y2 - rectangle3.y1) / 2);

        ctx.drawEllipse(ellipse1, "green");
        ellipse1.generatePoints(array, N);
        array.forEach(point => ctx.drawPoint(point, "blue"));
    }
} catch (e) {
    if (e instanceof DataError) {
        console.log(e);
    } else {
        console.log(e);
    }
}

